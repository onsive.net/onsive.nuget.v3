﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using OnSive.NuGet.v3.Classes;
using OnSive.NuGet.v3.Interfaces;

namespace OnSive.NuGet.v3
{
    public static class NuGet
    {
        public static INuGetClient Client { get; set; }

        public static async Task<NuGetResources> Get_Resources_Async() => await Client.Get_Resources_Async();

        public static async Task<NuGetPackageVersions> Get_PackageVersions_Asyn(string sPackageName) => await Client.Get_PackageVersions_Asyn(sPackageName);

        public static async Task<Stream> Get_PackageFile_Async(string sPackageName, string sVersion) => await Client.Get_PackageFile_Async(sPackageName, sVersion);

        public static async Task<Stream> Get_PackageManifestFile_Async(string sPackageName, string sVersion) => await Client.Get_PackageManifestFile_Async(sPackageName, sVersion);

        public static async Task<T> Send_Request<T>(string sResourceID, bool bDontSetExtension, params string[] sValues) => await Client.Send_Request_Async<T>(sResourceID, bDontSetExtension, sValues);

        public static async Task<HttpStatusCode> Delete_Package_Async(string sPackageName, string sVersion) => await Client.Delete_Package_Async(sPackageName, sVersion);
    }
}
