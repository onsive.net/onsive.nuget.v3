﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using OnSive.NuGet.v3.Classes;
using OnSive.NuGet.v3.Interfaces;
using SoftTec.Common.Protokoll;

namespace OnSive.NuGet.v3
{
    public class NuGetClient : INuGetClient, IDisposable
    {
        #region public Propertys
        public string sBaseAdress { get; }

        public NuGetResources Resources { get; }
        #endregion

        private HttpClient Client;
        private bool bIsDisposed;

        public NuGetClient(HttpClient Client, string sBaseAdress, NuGetResources Resources)
        {
            this.sBaseAdress = sBaseAdress;
            this.Resources = Resources;
            this.Client = Client;
        }

        private string Get_Url(string sResourceID, bool bDontSetExtension, params string[] sValues)
        {
            string sAddress = $"{Get_BaseUrl(sResourceID)}/";
            for (int i = 0; i < sValues.Length; i++)
            {
                sAddress += $"{sValues[i]}/";
            }
            if (!bDontSetExtension)
            {
                sAddress = $"{sAddress}index.json";
            }
            else
            {
                sAddress = sAddress.Substring(0, sAddress.Length - 1);
            }
            Proto.Debug($"{nameof(sAddress)}:{sAddress}");
            return sAddress;
        }

        private string Get_BaseUrl(string sResourceID)
        {
            if (string.IsNullOrEmpty(sResourceID))
            {
                return sBaseAdress;
            }
            IEnumerable<NuGetResource> Enumerable_Resources = Resources.oResources.Where(x => x.sType.Contains(sResourceID));
            if (Enumerable_Resources.Count() == 0)
            {
                throw new NuGetResourceException(sResourceID, $"The NuGet server does not support resource '{sResourceID}'!");
            }
            return Enumerable_Resources.ElementAt(0).sID;
        }

        public async Task<T> Send_Request_Async<T>(string sResourceID, bool bDontSetExtension, params string[] sValues)
        {
            HttpResponseMessage oHttpResponseMessage = await Client.GetAsync(Get_Url(sResourceID, bDontSetExtension, sValues));
            if (oHttpResponseMessage.IsSuccessStatusCode)
            {
                Proto.Debug($"Request Successfull");
                return JsonConvert.DeserializeObject<T>(await oHttpResponseMessage.Content.ReadAsStringAsync());
            }
            else
            {
                Proto.Debug($"Request Failed");
                throw new NuGetException(Get_Url(sResourceID, bDontSetExtension, sValues), $"Server responded with code {oHttpResponseMessage.StatusCode}");
            }
        }

        public async Task<Stream> Send_RequestStream_Async(string sResourceID, bool bDontSetExtension, params string[] sValues)
        {
            HttpResponseMessage oHttpResponseMessage = await Client.GetAsync(Get_Url(sResourceID, bDontSetExtension, sValues));
            if (oHttpResponseMessage.IsSuccessStatusCode)
            {
                Proto.Debug($"Request Successfull");
                return await oHttpResponseMessage.Content.ReadAsStreamAsync();
            }
            else
            {
                Proto.Debug($"Request Failed");
                throw new NuGetException(Get_Url(sResourceID, bDontSetExtension, sValues), $"Server responded with code {oHttpResponseMessage.StatusCode}");
            }
        }

        public async Task<HttpStatusCode> Send_Delete_Async(string sResourceID, bool bDontSetExtension, params string[] sValues)
        {
            HttpResponseMessage oHttpResponseMessage = await Client.SendAsync(new HttpRequestMessage(HttpMethod.Delete, Get_Url(sResourceID, bDontSetExtension, sValues)));
            return oHttpResponseMessage.StatusCode;
        }

        #region INuGetClient
        public async Task<NuGetResources> Get_Resources_Async()
        {
            return await Send_Request_Async<NuGetResources>(string.Empty, false);
        }

        public async Task<NuGetPackageVersions> Get_PackageVersions_Asyn(string sPackageName)
        {
            return await Send_Request_Async<NuGetPackageVersions>("PackageBaseAddress", false, sPackageName.ToLowerInvariant());
        }

        public async Task<Stream> Get_PackageFile_Async(string sPackageName, string sVersion)
        {
            return await Send_RequestStream_Async("PackageBaseAddress", true, sPackageName.ToLowerInvariant(), sVersion.ToLowerInvariant(), $"{sPackageName}.{sVersion}.nupkg".ToLowerInvariant());
        }

        public async Task<Stream> Get_PackageManifestFile_Async(string sPackageName, string sVersion)
        {
            return await Send_RequestStream_Async("PackageBaseAddress", true, sPackageName.ToLowerInvariant(), sVersion.ToLowerInvariant(), $"{sPackageName}.nuspec".ToLowerInvariant());
        }

        public async Task<HttpStatusCode> Delete_Package_Async(string sPackageName, string sVersion)
        {
            return await Send_Delete_Async("PackagePublish", false, sPackageName.ToLowerInvariant(), sVersion);
        }
        #endregion

        #region IDisposable
        protected virtual void Dispose(bool disposing)
        {
            if (!bIsDisposed)
            {
                if (disposing)
                {
                    Client.Dispose();
                }
                bIsDisposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
