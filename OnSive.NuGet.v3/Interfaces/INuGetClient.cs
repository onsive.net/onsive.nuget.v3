﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using OnSive.NuGet.v3.Classes;

namespace OnSive.NuGet.v3.Interfaces
{
    public interface INuGetClient
    {
        Task<NuGetResources> Get_Resources_Async();

        Task<NuGetPackageVersions> Get_PackageVersions_Asyn(string sPackageName);

        Task<Stream> Get_PackageFile_Async(string sPackageName, string sPackageVersion);

        Task<Stream> Get_PackageManifestFile_Async(string sPackageName, string sPackageVersion);

        Task<T> Send_Request_Async<T>(string sResourceID , bool bDontSetExtension, params string[] sValues);

        Task<HttpStatusCode> Delete_Package_Async(string sPackageName, string sVersion);
    }
}
