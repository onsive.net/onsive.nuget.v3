﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using OnSive.NuGet.v3.Classes;
using OnSive.NuGet.v3.Interfaces;

namespace OnSive.NuGet.v3
{
    public static class NuGetClientFactory
    {
        public static async Task<INuGetClient> CreateClientAsync(string sBaseAdress)
        {
            try
            {
                if (sBaseAdress.EndsWith("/"))
                {
                    sBaseAdress = sBaseAdress.Substring(0, sBaseAdress.Length - 1);
                }

                HttpClient Client = new HttpClient();
                NuGetResources oNuGetResources;

                HttpResponseMessage oHttpResponseMessage = await Client.GetAsync($"{sBaseAdress}/index.json");
                if (oHttpResponseMessage.IsSuccessStatusCode)
                {
                    oNuGetResources = JsonConvert.DeserializeObject<NuGetResources>(await oHttpResponseMessage.Content.ReadAsStringAsync());
                }
                else
                {
                    throw new NuGetException($"{sBaseAdress}/index.json", "Unaible to fetch the NuGet server resources.");
                }

                return new NuGetClient(Client, sBaseAdress, oNuGetResources);
            }
            catch (Exception ex)
            {
                throw new NuGetException($"{sBaseAdress}/index.json", "Internal error", ex);
            }
        }
    }
}
