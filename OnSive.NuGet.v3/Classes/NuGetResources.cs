﻿using System;
using Newtonsoft.Json;
using OnSive.NuGet.v3.Classes;

namespace OnSive.NuGet.v3.Classes
{
    public class NuGetResources
    {
        [JsonProperty("version")]
        public string sVersion { get; set; }

        [JsonProperty("resources")]
        public NuGetResource[] oResources { get; set; }
    }
}
