﻿using System;
using Newtonsoft.Json;

namespace OnSive.NuGet.v3.Classes
{
    public class NuGetResource
    {
        [JsonProperty("@id")]
        public string sID { get; set; }

        [JsonProperty("@type")]
        public string sType { get; set; }

        [JsonProperty("comment")]
        public string sComment { get; set; }
    }
}
