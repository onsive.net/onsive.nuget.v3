﻿using System;

namespace OnSive.NuGet.v3.Classes
{
    public class NuGetException : Exception
    {
        public string Url { get; }

        public NuGetException(string Url) : base()
        {
            this.Url = Url;
        }

        public NuGetException(string Url, string message) : base(message)
        {
            this.Url = Url;
        }

        public NuGetException(string Url, string message, Exception innerException) : base(message, innerException)
        {
            this.Url = Url;
        }
    }
}
