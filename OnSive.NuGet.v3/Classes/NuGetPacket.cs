﻿using System;
using Newtonsoft.Json;

namespace OnSive.NuGet.v3.Classes
{
    public class NuGetPackageVersions
    {
        [JsonProperty("versions")]
        public string[] sVersions { get; set; }
    }
}
