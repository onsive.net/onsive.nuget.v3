﻿using System;

namespace OnSive.NuGet.v3.Classes
{
    public class NuGetResourceException : Exception
    {
        public string ResourceID { get; }

        public NuGetResourceException(string ResourceID) : base()
        {
            this.ResourceID = ResourceID;
        }

        public NuGetResourceException(string ResourceID, string message) : base(message)
        {
            this.ResourceID = ResourceID;
        }

        public NuGetResourceException(string ResourceID, string message, Exception innerException) : base(message, innerException)
        {
            this.ResourceID = ResourceID;
        }
    }
}
