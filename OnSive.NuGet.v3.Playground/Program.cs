﻿using System;
using System.IO;
using System.Threading.Tasks;
using SoftTec.Common.Protokoll;
using SoftTec.Common.Protokoll.Enums;
using SoftTec.Common.Protokoll.Klassen;

namespace OnSive.NuGet.v3.Playground
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            Proto.Logger = new LoggerConfiguration()
                .AddSinkConsole(MinProtoLevel: enProtokollLevel.Verbose).CreateLogger();

            try
            {
                NuGet.Client = await NuGetClientFactory.CreateClientAsync("http://localhost:5000/v3/");

                var a = await NuGet.Delete_Package_Async("SoftTec.Common.Protokoll", "2.0.2");

              //  var a = await NuGet.Send_Request<object>("PackageBaseAddress", true, "softtec.common.funktionen","2.0.1","softtec.common.funktionen.2.0.1.nuspec");

                return 0;
            }
            catch (Exception ex)
            {
                Proto.Error(ex);
                return ex.HResult;
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
